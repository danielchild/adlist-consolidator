#!/usr/bin/env python3
import requests

adListurls = './adlists.txt'
outputList = './outputAdList.txt'

masterList = list()

urlsFound = 0
duplicates = 0

with open(adListurls, "r") as urls:
    for url in urls:
        url = url.rstrip('\r\n')
        try:
            adlist = requests.get(url, allow_redirects=True)
        except:
            continue
        

        for line in adlist.text.splitlines():
            
            if '#' in line:
                continue

            urlsFound += 1
            if line not in masterList:
                masterList.append(line)
            else:
                duplicates += 1
file = open(outputList, 'w', encoding="utf-8")
for line in masterList:
    file.write(line+'\n')
file.close()


print(f"Urls found: {urlsFound}")
print(f"Duplicates: {duplicates}")
print(f"Unique urls: {urlsFound-duplicates}")