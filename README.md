# adlist consolidator
`consolidator.py` takes all of your lists and consolidates them into one list. Its not efficiant but it works.

## Getting started
At the top of the script are two variables:
```py
adListurls = './adlists.txt'
outputList = './outputAdList.txt'
```
`adListurls` is the path to a text file that contains the urls to your ad lists. An example would be:
```
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADhosts.txt
https://www.github.developerdan.com/hosts/lists/dating-services-extended.txt
```
`outputList` is the path to where you want the output file to be.
